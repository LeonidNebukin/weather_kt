package com.example.weather.rest

import com.example.weather.pojo.WeatherInfo
import com.example.weather.core.Preferences
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query

interface WeatherREST {
    @GET("weather")
    fun getWeatherByCord(@Query("lat") lat : Double,
                         @Query("lon") lon : Double,
                         @Query("appid") key : String = Preferences.API_KEY_WEATHER,
                         @Query("units") value : String = "metric"): Call<WeatherInfo>
}