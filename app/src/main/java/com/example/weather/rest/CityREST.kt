package com.example.weather.rest

import com.example.weather.pojo.GeoObjectResponse
import com.example.weather.core.Preferences
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query


interface CityREST {
    @GET("1.x/")
    fun getCityList(@Query("apikey") key : String = Preferences.API_KEY_CITY,
                    @Query("geocode") name : String,
                    @Query("format") value : String = "json"): Call<GeoObjectResponse>
}