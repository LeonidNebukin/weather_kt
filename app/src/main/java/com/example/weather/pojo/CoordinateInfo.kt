package com.example.weather.pojo

import android.annotation.SuppressLint
import android.location.LocationManager

class CoordinateInfo(var lon : Double = 0.0, var lat : Double = 0.0) {
    @SuppressLint("MissingPermission")
    fun updateCoordinate(locationMan : LocationManager?) {
        val providers: List<String> =
            locationMan?.getProviders(true) ?: emptyList()
        for (provider in providers) {
            val location = locationMan?.getLastKnownLocation(provider)
            location?.let {
                lon = location.longitude
                lat = location.latitude
            }
        }
    }

    fun isDefault() : Boolean {
        return lon == 0.0 && lat == 0.0
    }
}