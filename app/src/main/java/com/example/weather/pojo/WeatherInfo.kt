package com.example.weather.pojo

class WeatherInfo (
    val coord : CoordinateInfo,
    val weather : Array<Weather>,
    val base : String,
    val main : Temperature,
    val visibility : Int,
    val wind : Wind,
    val clouds : Clouds,
    val dt : Long,
    val sys : Sys,
    val timezone: Long,
    val id : Int,
    val name : String,
    val cod : Int
    ) {

    inner class Weather(val id : Int, val main : String, val description : String, val icon : String) {}
    inner class Temperature(val temp : Double, val feels_like : Double, val temp_min : Double, val temp_max : Double, val pressure : Int, val humidity : Int) {}
    inner class Wind(val speed : Double, val deg : Double) {}
    inner class Clouds(val all : Int)
    inner class Sys(val type : Int, val id : Int, val message : Double, val country : String, val sunrise : Long, val sunset : Long)
}