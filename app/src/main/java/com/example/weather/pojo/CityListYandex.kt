package com.example.weather.pojo

import com.google.gson.annotations.SerializedName

data class GeoObject(
    @SerializedName("metaDataProperty")
    val metaDataProperty: MetaDataProperty,

    @SerializedName("name")
    val name: String,

    @SerializedName("description")
    val description: String?,

    @SerializedName("Point")
    val point: Point
)

data class GeoObjectResponse(
    @SerializedName("response")
    val response: Response
)

data class Response(
    @SerializedName("GeoObjectCollection")
    val geoObjectCollection: GeoObjectCollection
)

data class GeoObjectCollection(
    @SerializedName("metaDataProperty")
    val metaDataProperty: MetaDataProperty,

    @SerializedName("featureMember")
    val featureMember: List<FeatureMember>
)

data class FeatureMember(
    @SerializedName("GeoObject")
    val geoObject: GeoObject
)

data class Component(
    @SerializedName("kind")
    val kind: String,

    @SerializedName("name")
    val name: String
)

data class Address(
    @SerializedName("Components")
    val components: List<Component> = emptyList(),

    @SerializedName("formatted")
    val formattedAddress: String = ""
)

data class GeocoderMetaData(
    @SerializedName("Address")
    val address: Address = Address()
)

data class MetaDataProperty(
    @SerializedName("GeocoderMetaData")
    val geocoderMetaData: GeocoderMetaData = GeocoderMetaData(),

    @SerializedName("GeocoderResponseMetaData")
    val geocoderResponseMetaData : GeocoderResponseMetaData = GeocoderResponseMetaData()
)

data class Point(
    @SerializedName("pos")
    val pos: String
)

data class GeocoderResponseMetaData(
    @SerializedName("request")
    val request: String = "",
    @SerializedName("found")
    val found : String = "",
    @SerializedName("results")
    val results : String = ""
)