package com.example.weather.core

object Preferences {
    const val SharedPref = "WEATHER_PREF"
    const val PreferencesCityLon = "CITY_LON"
    const val PreferencesCityLat = "CITY_LAT"
    const val API_KEY_WEATHER = "3a88a4982b6a3b15de3754c42c8cfb7c"
    const val URL_Server_WEATHER = "https://api.openweathermap.org/data/2.5/"
    const val HTML_BY_CENTER_FORMAT = "<html><body style=\"text-align: center; background-color: null; vertical-align: middle;\"><img src = \"%s\" /></body></html>"
    const val URL_Server_CITY = "https://geocode-maps.yandex.ru/"
    const val API_KEY_CITY = "3e75a9e8-3efa-4b30-8866-b02004d1ef84"
}