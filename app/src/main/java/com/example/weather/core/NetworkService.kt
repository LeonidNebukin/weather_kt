package com.example.weather.core

import com.example.weather.rest.CityREST
import com.example.weather.rest.WeatherREST
import retrofit2.Retrofit
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory


class NetworkService {
    @Volatile private var mWeatherREST : WeatherREST? = null
    @Volatile private var mCityRest : CityREST? = null

    fun getInstanceWeather() : WeatherREST =
        mWeatherREST ?: synchronized(this) {
            mWeatherREST ?: Retrofit.Builder()
                .baseUrl(Preferences.URL_Server_WEATHER)
                .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .build()
                .create(WeatherREST::class.java).also { mWeatherREST = it }
        }

    fun getInstanceCity() : CityREST =
        mCityRest ?: synchronized(this) {
            mCityRest ?: Retrofit.Builder()
                .baseUrl(Preferences.URL_Server_CITY)
                .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .build()
                .create(CityREST::class.java).also { mCityRest = it }
        }
}