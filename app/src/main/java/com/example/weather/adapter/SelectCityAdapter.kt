package com.example.weather.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import com.example.weather.R
import com.example.weather.pojo.FeatureMember
import com.example.weather.core.NetworkService

class SelectCityAdapter(private val context : Context) : BaseAdapter(), Filterable {

    private var filterList : List<FeatureMember> = emptyList()

    override fun getFilter(): Filter? = FilterClass()

    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {
        val item = getItem(position)
        val view = convertView ?:
            LayoutInflater.from(context).inflate(R.layout.autocomplete_layout_dropdowm_item, parent, false)

        return view.apply {
            findViewById<TextView>(R.id.nameCity).text = item.geoObject.name
            findViewById<TextView>(R.id.description).text = item.geoObject.description
        }
    }

    override fun getItem(position: Int): FeatureMember {
        return filterList[position]
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    override fun getCount(): Int {
        return filterList.size
    }

    inner class FilterClass : Filter() {
        override fun performFiltering(constraint: CharSequence?): FilterResults {
            val filterResults = FilterResults()
            constraint?.let {
                filterList = NetworkService().
                    getInstanceCity().
                    getCityList(name = it.toString()).
                    execute().
                    body()?.
                    response?.geoObjectCollection?.featureMember ?: emptyList()

                filterResults.apply {
                    values = filterList
                    count = filterList.size
                }
            }
            return filterResults
        }

        override fun publishResults(constraint: CharSequence?, results: FilterResults?) {
            if (results != null && results.count > 0) {
                notifyDataSetChanged()
            } else {
                notifyDataSetInvalidated()
            }
        }
    }
}