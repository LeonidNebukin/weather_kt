package com.example.weather.ui

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.AdapterView
import androidx.appcompat.app.AppCompatActivity
import com.example.weather.R
import com.example.weather.adapter.SelectCityAdapter
import com.example.weather.pojo.FeatureMember
import com.example.weather.core.Preferences
import kotlinx.android.synthetic.main.settings_activity.*


class SettingsActivity : AppCompatActivity(), AdapterView.OnItemClickListener  {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.settings_activity)

        city.apply {
            setAdapter(
                SelectCityAdapter(context = baseContext)
            )
            setOnItemClickListener { parent, view, position, id ->
                onItemClick(parent, view, position, id)
            }
        }
    }

    override fun onItemClick(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
        val selectedItem = parent?.getItemAtPosition(position) as FeatureMember
        city.apply {
            setText(selectedItem.geoObject.name)
            dismissDropDown()
        }

        val coord = selectedItem.geoObject.point.pos.split(" ")
        if (coord.size == 2) {
            updatePreferences(coord)
            setResult(
                RESULT_OK, Intent()
                    .putExtra(Preferences.PreferencesCityLon, coord[0].toDouble())
                    .putExtra(Preferences.PreferencesCityLat, coord[1].toDouble())
            )
        }
        finish()
    }

    private fun updatePreferences(coordinate : List<String>) {
        with(getSharedPreferences(Preferences.SharedPref, Context.MODE_PRIVATE).edit()) {
            putString(Preferences.PreferencesCityLon, coordinate[0])
            putString(Preferences.PreferencesCityLat, coordinate[1])
            apply()
        }
    }
}
