package com.example.weather.ui

import android.Manifest
import android.annotation.SuppressLint
import android.app.Activity
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.location.LocationManager
import android.os.Build
import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import com.example.weather.R
import com.example.weather.pojo.CoordinateInfo
import com.example.weather.pojo.WeatherInfo
import com.example.weather.core.NetworkService
import com.example.weather.core.Preferences
import kotlinx.android.synthetic.main.weather_activity.*
import retrofit2.Call
import retrofit2.Response
import javax.inject.Inject

class WeatherActivity : AppCompatActivity() {
    @Inject
    lateinit var coordinate: CoordinateInfo

    private var curWeather: WeatherInfo? = null

    private var locationMan : LocationManager? = null
    private val cityIdRequest: Int = 1

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.weather_activity)

        val sharedPreferences = getSharedPreferences(Preferences.SharedPref, Context.MODE_PRIVATE)
        coordinate = CoordinateInfo(
                lon = sharedPreferences.getString(Preferences.PreferencesCityLon, "0.0")?.toDouble() ?: 0.0,
                lat = sharedPreferences.getString(Preferences.PreferencesCityLat, "0.0")?.toDouble() ?: 0.0
        )

        locationMan = getSystemService(Context.LOCATION_SERVICE) as LocationManager
        checkLocationPermission()

        settings_button?.setOnClickListener { startSettingsActivity() }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == cityIdRequest && resultCode == Activity.RESULT_OK) {
            coordinate.lat = data?.getDoubleExtra(Preferences.PreferencesCityLat, 0.0) ?: 0.0
            coordinate.lon = data?.getDoubleExtra(Preferences.PreferencesCityLon, 0.0) ?: 0.0
            updateWeather()
        }
    }

    private fun updateWeather() {
        when {
            !coordinate.isDefault() -> {
                NetworkService().
                    getInstanceWeather().
                    getWeatherByCord(lat = coordinate.lat, lon = coordinate.lon).
                    enqueue(WeatherCallback())
            }
            else -> {
                startSettingsActivity()
            }
        }
    }

    private fun updateWeathersView() {
        temp?.text =
            "${curWeather?.main?.temp}°C   ${curWeather?.main?.humidity}%"
        temp_feels_like?.text =
            "${getString(R.string.temp_feel_like)}:${curWeather?.main?.feels_like}°C"
        description?.text = curWeather?.weather?.get(0)?.description
        cityName?.text = curWeather?.name

        val html = java.lang.String.format(
            Preferences.HTML_BY_CENTER_FORMAT,
            "https://openweathermap.org/img/wn/${curWeather?.weather?.get(0)?.icon}@2x.png"
        )
        icon?.loadDataWithBaseURL("", html, "text/html", "UTF-8", "")
    }

    private fun startSettingsActivity() {
        startActivityForResult(
            Intent(this, SettingsActivity::class.java), cityIdRequest)
    }

    @SuppressLint("MissingPermission")
    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (requestCode == 10 && grantResults.isNotEmpty()
            && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            coordinate.updateCoordinate(locationMan)
            updateWeather()
        } else {
            //first start
            startSettingsActivity()
        }
    }

    override fun onPause() {
        super.onPause()
        updatePreferences()
    }

    private fun updatePreferences() {
        with(getSharedPreferences(Preferences.SharedPref, Context.MODE_PRIVATE).edit()) {
            putString(Preferences.PreferencesCityLat, coordinate.lat.toString())
            putString(Preferences.PreferencesCityLon, coordinate.lon.toString())
            apply()
        }
    }


    private fun checkLocationPermission() {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED &&
            Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            requestPermissions(
                arrayOf(Manifest.permission.ACCESS_FINE_LOCATION), 10)
        }
        else
        {
            updateWeather()
        }
    }

    inner class WeatherCallback : retrofit2.Callback<WeatherInfo?> {
        override fun onFailure(call: Call<WeatherInfo?>, t: Throwable) {
            Toast.makeText(baseContext,
                R.string.error_request, Toast.LENGTH_LONG).show()
        }

        override fun onResponse(call: Call<WeatherInfo?>, response: Response<WeatherInfo?>) {
            curWeather = response.body()
            updateWeathersView()
        }
    }
}
